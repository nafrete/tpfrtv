<?php

namespace AppBundle\Tests\Services;

use AppBundle\Document\Article;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ArticleServiceTest extends WebTestCase
{

    private static $articleService;
    private static $container;

    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        self::$container = $kernel->getContainer();
        self::$articleService = self::$container->get('article_service');
    }

    public function test_attributes_exists()
    {
        $this->assertClassHasAttribute('odm', 'AppBundle\Services\ArticleService');
    }

    public function test_get_repository()
    {
        $this->assertNotNull(self::$articleService->getRepository());
    }

    public function test_get_articles()
    {
        $this->assertInternalType('array', self::$articleService->getArticles());
    }

    public function test_save_article()
    {
        $article = new Article();
        $article->setBody("hello");
        $article->setTitle("Title");
        $article->setLeading("Test...");
        $article->setCreatedBy("Moi");

        $this->assertEmpty(self::$articleService->saveArticle($article));
    }

    public function test_save_false_article()
    {
        $article = new Article();
        $article->setLeading("Test...");
        $article->setCreatedBy("Moi");

        try {
            $this->assertEmpty(self::$articleService->saveArticle($article));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
        }
    }

    public function test_delete_false_article()
    {
        try {
            $this->assertEmpty(self::$articleService->deleteArticle(747));
        } catch (\Exception $e) {
            $this->assertNotNull($e->getMessage());
        }
    }
}