<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ArticleControllerTest extends WebTestCase
{
    public function test_api_get_articles()
    {
        $client = static::createClient();
        $client->request('GET', '/articles');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_get_false_article()
    {
        $client = static::createClient();
        $client->request('GET', '/articles/zefhizoie566556');
        $this->assertTrue(500 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    public function test_api_delete_false_application()
    {
        $client = static::createClient();
        $client->request('DELETE', '/articles/5zeTestd78');
        $this->assertTrue(500 === $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

}
