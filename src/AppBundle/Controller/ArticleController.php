<?php

namespace AppBundle\Controller;

use AppBundle\Document\Application;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\ArticleType;
use AppBundle\Document\Article;

class ArticleController extends FOSRestController
{

    /**
     * @DI\Inject("article_service")
     */
    private $articleService;

    /**
     * Display all articles
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the articles are not found"
     *     }
     * )
     */
    public function getArticlesAction()
    {
        $articles = $this->articleService->getArticles();

        $view = $this->view($articles, 200)
            ->setTemplate(":articles:listArticles.html.twig")
            ->setTemplateVar('articles');

        return $this->handleView($view);
    }


    /**
     * Display one article by his Id
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the article is not found"
     *     }
     * )
     * @param $slug
     * @return
     */
    public function getArticleAction($slug)
    {
        return $this->articleService->getArticle($slug);
    }

    /**
     * Post a new article
     *
     * @ApiDoc(
     *  statusCodes={
     *         200="Returned when successful",
     *         401="Unauthorized access"
     *  },
     *  input="AppBundle\Form\ArticleType",
     *  output="AppBundle\Document\Article"
     * )
     * @param Request $request
     * @return Response
     */
    public function postArticleAction(Request $request)
    {
        $form = $this->createForm(new ArticleType(), new Article());

        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {
            $article = $form->getData();

            $this->articleService->saveArticle($article);

            return $this->redirect($this->generateUrl('homepage'));
        }
    }

    /**
     * Delete one article by his Id
     *
     * @ApiDoc(
     *  resource=true,
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the article is not found"
     *  }
     * )
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteArticleAction($id)
    {
        $this->articleService->deleteArticle($id);

        return $this->redirect($this->generateUrl('homepage'));
    }

}
