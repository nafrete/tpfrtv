<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Form\ArticleType;
use AppBundle\Document\Article;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class DefaultController extends Controller
{

    /**
     * @DI\Inject("article_service")
     */
    private $articleService;

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $articles = $this->articleService->getArticles();

        return $this->render('articles/listArticles.html.twig', array(
            'articles' => $articles,
        ));
    }

    /**
     * @Route("/article/{slug}", name="show")
     * @ParamConverter("article", class="AppBundle:Article")
     */
    public function showAction($slug)
    {
        $article = $this->articleService->getArticle($slug);
        $deleteForm = $this->createDeleteForms($article->getId());

        return $this->render('articles/article.html.twig', array(
            'article' => $article,
            'deleteForm' => $deleteForm->createView(),
        ));
    }

    private function createDeleteForms($id)
    {
        return $this->createFormBuilder(array('id' => $id))
        ->add('id', 'hidden')
        ->getForm()
    ;
}

    /**
     * @Route("/creer", name="create")
     */
    public function createAction()
    {
        $article = new Article();
        $form = $this->createForm(new ArticleType(), $article);

        return $this->render('articles/create.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
