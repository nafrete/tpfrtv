<?php

namespace AppBundle\Services;

use Doctrine\ODM\MongoDB\DocumentManager as ODM;
use AppBundle\Document\Article;

class ArticleService
{

    private $odm;

    public function __construct(ODM $odm)
    {
        $this->odm = $odm;
    }

    public function getRepository()
    {
        return $this->odm->getRepository('AppBundle:Article');
    }

    public function getArticles()
    {
        return $this->getRepository()->findAll();
    }

    public function getArticle($slug)
    {
        return $this->getRepository()->findOneBySlug($slug);
    }

    public function saveArticle(Article $article)
    {
        $this->odm->persist($article);
        $this->odm->flush();
    }

    public function deleteArticle($id)
    {
        $article = $this->getRepository()->findOneById($id);
        $this->odm->remove($article);
        $this->odm->flush();
    }

}